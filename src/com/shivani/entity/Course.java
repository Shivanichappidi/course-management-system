package com.shivani.entity;


import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Courses")
public class Course {
	
	private Long id;
	
	private String courseName;
	
	
	
	private String duration;
	
	private Double fee;
	
	private Date startDate;
	
	private Date endDate;


}
