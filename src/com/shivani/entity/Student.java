package com.shivani.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Students")
public class Student {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length=15)
	private String name;
	
	@Column(length=20)
	private String email;
	
	@Column(length=10)
	private Long contactNo;
	
	@Column(length=15)
	private String place;
	
	@Column(length=10)
	private String qualification;
	
	
	
	
	

}
