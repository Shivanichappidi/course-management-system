package com.shivani.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Enrollments")
public class Enrollment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length=10)
	private Date enrollmentDate;
	
	@Column(length=10)
	private String enrollmentStatus;
	
	Student studentFk;
	
	Course courseFk;
	
	
	

}
